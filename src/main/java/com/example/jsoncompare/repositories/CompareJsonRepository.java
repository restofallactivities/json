package com.example.jsoncompare.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.jsoncompare.models.Json;

@Repository
public interface CompareJsonRepository extends JpaRepository<Json, Integer>{

}
