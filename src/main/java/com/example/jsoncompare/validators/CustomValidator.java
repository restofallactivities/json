package com.example.jsoncompare.validators;

import java.util.HashMap;
import java.util.Map;

import org.springframework.util.CollectionUtils;

import com.example.jsoncompare.constants.Constants;
import com.example.jsoncompare.models.JsonDiffResponse;
import com.example.jsoncompare.utils.FlatMapUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.MapDifference;
import com.google.common.collect.MapDifference.ValueDifference;
import com.google.common.collect.Maps;

public class CustomValidator {
	
	public static Map<String, Object> parseJSON(String jsonData) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		TypeReference<Map<String, Object>> type = new TypeReference<Map<String, Object>>() {};

		return mapper.readValue(jsonData, type);
	}
	
	public static Object getJsonDiff(Map<String, Object> baseMap, Map<String, Object> mapToCompare) {
		Map<String, Object> leftMap = FlatMapUtil.flatten(baseMap);
		Map<String, Object> rightMap = FlatMapUtil.flatten(mapToCompare);
		
		MapDifference<String, Object> diff = Maps.difference(leftMap, rightMap);
		if(diff.areEqual()) {
			return Constants.RESPONSE_SAME_JSON;
		}
		else {
			JsonDiffResponse response = new JsonDiffResponse();
			response.setOnlyInBase(diff.entriesOnlyOnLeft());
			response.setOnlyInComparing(diff.entriesOnlyOnRight());
			response.setEntriesCommon(diff.entriesInCommon());
			Map<String, String> difference = new HashMap<>();
			Map<String, ValueDifference<Object>> entriesDiffering = diff.entriesDiffering();
			if(!CollectionUtils.isEmpty(entriesDiffering)) {
				entriesDiffering.forEach((key, val)->{
					difference.put(key, val.toString());
				});
				response.setEntriesDiffering(difference);
			}
			
			return response;
		}
	}

}
