package com.example.jsoncompare.constants;

public class Constants {
	public static enum STATE {
		JSON_PARSE_FAILURE,
		SUCCESS,
		FAILURE
	}
	//URLs
	public static final String URL_MAIN = "/api/json";
	public static final String URL_BASE_JSON = "/base-json";
	public static final String URL_COMPARE_JSON = "/compare-json";
	public static final String URL_LOGIN = "/login";
	public static final String URL_H2CONSOLE = "/h2console/**";
	
	//Response
	public static final String RESPONSE_ADD_SUCCESS = "Base json added successfully.";
	public static final String RESPONSE_ADD_FAILURE = "Base json could not be added.";
	public static final String RESPONSE_INVALID_JSON = "Unparsable input json.";
	public static final String RESPONSE_UPDATE_SUCCESS = "Base json updated successfully.";
	public static final String RESPONSE_UPDATE_FAILURE = "Base json could not be updated.";
	public static final String RESPONSE_DELETE_SUCCESS = "Base json deleted successfully.";
	public static final String RESPONSE_DELETE_FAILURE = "Base json could not be deleted.";
	public static final String RESPONSE_SAME_JSON = "Both the JSON objects are equal.";
	public static final String RESPONSE_INVALID_BASE_JSON = "Invalid id of Base JSON passed.";
	
	//User
	public static final String USERNAME = "user1";
	
	public static final String USER_DISABLED = "USER_DISABLED";
	public static final String INVALID_CREDENTIALS = "INVALID_CREDENTIALS";
}
