package com.example.jsoncompare.models;

import java.io.Serializable;

public class JsonCompare implements Serializable{
	
	private static final long serialVersionUID = -6380478372992062191L;
	private int baseJsonId;
	private String jsonToCompare;
	
	public int getBaseJsonId() {
		return baseJsonId;
	}
	public void setBaseJsonId(int baseJsonId) {
		this.baseJsonId = baseJsonId;
	}
	public String getJsonToCompare() {
		return jsonToCompare;
	}
	public void setJsonToCompare(String jsonToCompare) {
		this.jsonToCompare = jsonToCompare;
	}
}
