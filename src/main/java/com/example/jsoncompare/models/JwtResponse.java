package com.example.jsoncompare.models;

import java.io.Serializable;

public class JwtResponse implements Serializable{

	private static final long serialVersionUID = -8857092502042427549L;

	private final String jwttoken;
	
	public JwtResponse(String jwttoken) {
		this.jwttoken = jwttoken;
	}

	public String getToken() {
		return this.jwttoken;
	}
}
