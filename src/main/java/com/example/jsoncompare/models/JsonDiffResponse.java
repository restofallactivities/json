package com.example.jsoncompare.models;

import java.io.Serializable;
import java.util.Map;

public class JsonDiffResponse implements Serializable{

	private static final long serialVersionUID = 8744406528991534622L;
	private Map<String, Object> onlyInBase;
	private Map<String, Object> onlyInComparing;
	private Map<String, String> entriesDiffering;
	private Map<String, Object> entriesCommon;
	
	public Map<String, Object> getOnlyInBase() {
		return onlyInBase;
	}
	public void setOnlyInBase(Map<String, Object> onlyInBase) {
		this.onlyInBase = onlyInBase;
	}
	public Map<String, Object> getOnlyInComparing() {
		return onlyInComparing;
	}
	public void setOnlyInComparing(Map<String, Object> onlyInComparing) {
		this.onlyInComparing = onlyInComparing;
	}
	public Map<String, Object> getEntriesCommon() {
		return entriesCommon;
	}
	public void setEntriesCommon(Map<String, Object> entriesCommon) {
		this.entriesCommon = entriesCommon;
	}
	public Map<String, String> getEntriesDiffering() {
		return entriesDiffering;
	}
	public void setEntriesDiffering(Map<String, String> entriesDiffering) {
		this.entriesDiffering = entriesDiffering;
	}
	
	@Override
	public String toString() {
		return "JsonDiffResponse [onlyInBase=" + onlyInBase + ", onlyInComparing=" + onlyInComparing
				+ ", entriesDiffering=" + entriesDiffering + ", entriesCommon=" + entriesCommon + "]";
	}	
}
