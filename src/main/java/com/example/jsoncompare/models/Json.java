package com.example.jsoncompare.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Json{

	@Id
	@GeneratedValue
	private int id;
	
	@Column(name = "base_json")
	private String baseJson;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBaseJson() {
		return baseJson;
	}

	public void setBaseJson(String baseJson) {
		this.baseJson = baseJson;
	}

	@Override
	public String toString() {
		return "Json [id=" + id + ", baseJson=" + baseJson + "]";
	}
	
	
}
