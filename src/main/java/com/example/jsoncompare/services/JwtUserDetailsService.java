package com.example.jsoncompare.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.jsoncompare.constants.Constants;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	@Value("${jwt.secret}")
	private String secret;

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		/* This username is hard-coded in Constants.java because reading from application.properties
		 * is creating the username with some hash value and that value is different from 
		 * the one coming as a parameter to this function, hence equals method returns false. 
		 */
		if (Constants.USERNAME.equals(userName)) {
			return new User(Constants.USERNAME, secret, new ArrayList<>());
		} else {
			throw new UsernameNotFoundException("User not found with username: " + userName);
		}
	}
}
