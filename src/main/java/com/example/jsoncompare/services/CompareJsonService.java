package com.example.jsoncompare.services;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.example.jsoncompare.constants.Constants;
import com.example.jsoncompare.models.Json;
import com.example.jsoncompare.models.JsonCompare;
import com.example.jsoncompare.repositories.CompareJsonRepository;
import com.example.jsoncompare.validators.CustomValidator;
import com.fasterxml.jackson.core.JsonProcessingException;

@Service
public class CompareJsonService {
	
	@Autowired
	private CompareJsonRepository repo;
	
	public List<Json> getBaseJson() {
		List<Json> baseJsonList = repo.findAll();
		return !CollectionUtils.isEmpty(baseJsonList) ? baseJsonList : null;
	}
	
	public Optional<Json> getBaseJsonById(int id) {
		return repo.findById(id);
	}
	
	public ResponseEntity<String> addNewBaseJson(Json baseJson) {
		
		try{
			CustomValidator.parseJSON(baseJson.getBaseJson());
			repo.save(baseJson);
			return getResponseString(Constants.RESPONSE_ADD_SUCCESS, HttpStatus.CREATED);
		}
		catch(JsonProcessingException ex) {
			return getResponseString(Constants.RESPONSE_INVALID_JSON, HttpStatus.BAD_REQUEST);
		}
		catch(Exception ex) {
			ex.printStackTrace();
			return getResponseString(Constants.RESPONSE_ADD_FAILURE, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	public ResponseEntity<String> saveBaseJson(Json baseJson) {
		try{
			CustomValidator.parseJSON(baseJson.getBaseJson());
			repo.save(baseJson);
			return getResponseString(Constants.RESPONSE_UPDATE_SUCCESS, HttpStatus.OK);
		}
		catch(JsonProcessingException ex) {
			return getResponseString(Constants.RESPONSE_INVALID_JSON, HttpStatus.BAD_REQUEST);
		}
		catch(Exception ex) {
			ex.printStackTrace();
			return getResponseString(Constants.RESPONSE_ADD_FAILURE, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	public ResponseEntity<String> deleteBaseJsonById(int id) {
		try{
			repo.deleteById(id);
			return getResponseString(Constants.RESPONSE_DELETE_SUCCESS, HttpStatus.OK);
		}
		catch(Exception ex) {
			ex.printStackTrace();
			return getResponseString(Constants.RESPONSE_DELETE_FAILURE, HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}
	
	public Object compareJson(JsonCompare json) {		
		Map<String, Object> mapToCompare = null;
		Map<String, Object> mapBaseJson = null;
		try {
			mapToCompare = CustomValidator.parseJSON(json.getJsonToCompare());
			Optional<Json> baseJson= getBaseJsonById(json.getBaseJsonId());
			if(!baseJson.isPresent()) {
				return getResponseString(Constants.RESPONSE_INVALID_BASE_JSON, HttpStatus.BAD_REQUEST);
			}
			else {
				mapBaseJson = CustomValidator.parseJSON(baseJson.get().getBaseJson());
			}
			
			return CustomValidator.getJsonDiff(mapBaseJson, mapToCompare);
		} catch (JsonProcessingException e) {
			return getResponseString(Constants.RESPONSE_INVALID_JSON, HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return getResponseString(Constants.RESPONSE_INVALID_JSON, HttpStatus.BAD_REQUEST);
		}
	}

	private ResponseEntity<String> getResponseString(String message, HttpStatus status) {
		return new ResponseEntity<String>(message, status);
	}

}
