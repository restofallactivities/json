package com.example.jsoncompare.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.jsoncompare.constants.Constants;
import com.example.jsoncompare.models.Json;
import com.example.jsoncompare.models.JsonCompare;
import com.example.jsoncompare.services.CompareJsonService;

@RestController
@RequestMapping(Constants.URL_MAIN)
public class CompareJsonRestController {

	@Autowired
	private CompareJsonService service;
	
	@GetMapping(Constants.URL_BASE_JSON)
	public List<Json> getBaseJson() {
		return service.getBaseJson(); 
	}
	
	@PostMapping(Constants.URL_BASE_JSON)
	public ResponseEntity<String> addNewBaseJson(@RequestBody Json baseJson) {
		return service.addNewBaseJson(baseJson);
	}
	
	@PutMapping(Constants.URL_BASE_JSON)
	public ResponseEntity<String> updateBaseJson(@RequestBody Json baseJson) {
		return service.saveBaseJson(baseJson);
	}
	
	@PutMapping(Constants.URL_BASE_JSON + "/{id}")
	public ResponseEntity<String> updateBaseJsonWithIdInURL(@PathVariable("id") int id, @RequestBody Json baseJson) {
		baseJson.setId(id);
		return service.saveBaseJson(baseJson);
	}
	
	@DeleteMapping(Constants.URL_BASE_JSON + "/{id}")
	public ResponseEntity<String> deleteBaseJsonById(@PathVariable("id") int id) {
		return service.deleteBaseJsonById(id);
	}
	
	@PostMapping(Constants.URL_COMPARE_JSON)
	public Object compareJson(@RequestBody JsonCompare jsonCompare) {
		return service.compareJson(jsonCompare);
	}
	
}
