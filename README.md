/***  JSON Compare  ***/

-  Once Application is started, it runs on port 8080.
   It is created with H2 database. It is accessible using below URL
   http://localhost:8080/h2console
   Username: sa, Password: password

-  data.sql has all the required scripts to create a DB table.
   Table Name: JSON
   Table Column names: 1. ID  2. BASE_JSON

-  User needs to login first using below details to generate the JWT token
   http://localhost:8080/login
   Username: user1, Password: password
   
-  Once the token is available, base json can be inserted in the DB.
   There can be multiple entries for Base Json in the DB which can be used to different comparisons.
   URL for base json operations are as below:
   GET : "http://localhost:8080/api/json/base-json"
   POST (add new) : "http://localhost:8080/api/json/base-json"    Payload: {"baseJson": "{\"employee\": { \"ha\": [2, 312,33]}}"}
   PUT (update) : "http://localhost:8080/api/json/base-json"    Payload: {"id": 1, "baseJson": "{\"employee\": { \"ha\": [2, 312,33]}}"}
   PUT (update) : "http://localhost:8080/api/json/base-json/1"    Payload: {"baseJson": "{\"employee\": { \"ha\": [2, 312,33]}}"}
   DELETE (delete) : "http://localhost:8080/api/json/base-json/1"   
   POST (compare) : "http://localhost:8080/api/json/compare-json" Payload: {"baseJsonId": 2, "jsonToCompare": "{\"employee\": { \"ha\": [11, 33,33], \"h1\": 44 }}
   For comparing, we need to provide base-json id from DB and the json to compare with base-json
   Difference in json is found using Guava library MapDifference class
   
- test changes 3
   
